# Miscellaneous Prediction Project



### Dataset
- All dataset is in all_data.zip
- The given dataset is file `dataset.csv`
- All other datasets are generated during the training processes.
### Flow
Detail flow is shown in `slide.pptx` including
- EDA: `common.ipynb` and `EDA.ipynb`
- Modeling: `modeling for x features.ipynb` and `modeling y1, y2.ipynb`
- Feature engineering
- Model evaluation
